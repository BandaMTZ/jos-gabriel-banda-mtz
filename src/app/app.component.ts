import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<h1>HOLA {{name}}</h1> <img [src]="url">`,

  styles: [
    `h1 {color: red;}`,
    `img {width: 40%; height: 400px;}`
  ]
})

export class AppComponent  { 
  name = 'José Gabriel Banda Martínez';
  url = "http://static2.elblogverde.com/wp-content/uploads/2015/10/tigre-de-bengala-panthera-tigris-tigris-600x328.jpg";
 }
